[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-blue.svg?style=for-the-badge)](https://creativecommons.org/licenses/by-sa/4.0/)
[![made-with-ruby](https://img.shields.io/badge/Made%20with-Ruby-blue.svg?style=for-the-badge)](https://www.ruby-lang.org)

# Expanding Hashes

## Showing additional messages (debug)
```shell
export APP_ENV='dev'
rspec
```
