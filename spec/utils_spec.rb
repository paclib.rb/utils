require 'test_helper'

describe "utils" do
  context ".resolve (using symbols for keys)" do
    context 'should fail if an invalid parameter is provided' do
      before do
        @input = "radom value that is not a hash"
      end
      subject { resolve(@input) }
      it { is_expected.to be false }
    end

    context 'should expand hash based on linked keys' do
      before do
        @input = {
          key_1: {
            subkey_1: '1_value',
            subkey_2: '2_value',
            subkey_3: '3_value'
          },
          key_2: {
            _: :key_1, # INFO: mandatory to identify linked content
            subkey_1: '1_value_replaced',
            _subkey_3: '3_value_merged', # INFO: prefix not required here, but allowed, since value is not a hash = no need for recursion
            subkey_4: '4_value_added'
          }
        }
        @expected = {
          key_1: {
            subkey_1: '1_value',
            subkey_2: '2_value',
            subkey_3: '3_value'
          },
          key_2: {
            subkey_1: '1_value_replaced',
            subkey_2: '2_value',
            subkey_3: '3_value_merged',
            subkey_4: '4_value_added'
          }
        }
      end
      subject { resolve(@input) }
      it { is_expected.to eq(@expected) }
    end

    context 'should expand current level merging values if hashes' do
      before do
        @input = {
          key_1: {
            subkey_1: '1_value',
            subkey_2: '2_value',
            subkey_3: { foo: 'bar' }
          },
          key_2: {
            _: :key_1,
            subkey_1: '1_value_replaced',
            _subkey_3: { foo: 'bar2' },
            subkey_4: '4_value_added'
          }
        }
        @expected = {
          key_1: {
            subkey_1: '1_value',
            subkey_2: '2_value',
            subkey_3: { foo: 'bar' }
          },
          key_2: {
            subkey_1: '1_value_replaced',
            subkey_2: '2_value',
            subkey_3: { foo: 'bar2' },
            subkey_4: '4_value_added'
          }
        }
      end
      subject { resolve(@input) }
      it { is_expected.to eq(@expected) }
    end

    context 'should also expand sub-hashes (aka deep expand)' do
      before do
        @input = {
          key_1: {
            subkey_1: '1_value',
            subkey_2: '2_value',
            subkey_3: { foo: 'bar', bar: 'foo' }
          },
          key_2: {
            _: :key_1,
            subkey_1: '1_value_replaced',
            _subkey_3: { foo: 'bar2' },
            subkey_4: '4_value_added'
          }
        }
        @expected = {
          key_1: {
            subkey_1: '1_value',
            subkey_2: '2_value',
            subkey_3: { foo: 'bar', bar: 'foo' }
          },
          key_2: {
            subkey_1: '1_value_replaced',
            subkey_2: '2_value',
            subkey_3: { foo: 'bar2', bar: 'foo' },
            subkey_4: '4_value_added'
          }
        }
      end
      subject { resolve(@input) }
      it { is_expected.to eq(@expected) }
    end
  end
end
