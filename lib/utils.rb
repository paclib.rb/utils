require 'logger'

# LOGGER = Logger.new(STDERR, datetime_format: '%Y%m%d%H%M%S.%6N')
LOGGER = Logger.new(STDERR, datetime_format: '%H%M%S.%6N')
LOGGER.level = ENV['APP_ENV'] == 'dev' ? Logger::DEBUG : Logger::WARN

def resolve(p_hash, p_hash_path = nil)
  LOGGER.debug(__method__){ "Passed hash path: '#{p_hash_path}'" }
  l_hash_path = p_hash_path || [] # XXX: required to be able to push 'root' keys
  LOGGER.debug(__method__){ "Hash path is now '#{l_hash_path}'" }
  l_data = p_hash_path.nil? ? p_hash : l_data = p_hash.dig(*l_hash_path)
  return false unless l_data.is_a?(Hash) && !l_data.empty?
  # l_linked_to = l_data.fetch(:_, nil)
  l_linked_to = l_data.delete(:_)
  if !l_linked_to.nil?
    l_linked_path = l_hash_path.dup
    l_linked_path[-1] = l_linked_to
    LOGGER.debug(__method__){ "Hash path '#{l_hash_path}' is linked to path '#{l_linked_path}'" }
    # p l_hash_resolved = p_hash.dig(*l_linked_path).clone # NOK because of shallow copy (references seems to be shared = modifying l_hash_resolved will modify p_hash !!!) = need a real deep copy
    # XXX: best solution find until now = making a memory / binary dump ! aka serializing as a stream of bytes and making a copy of it before reloading it ... having obtained a REAL deep copy ... TODO: cost = refactor / alternative ?
    # SEE: https://ruby-doc.org/core/Marshal.html
    l_hash_resolved = Marshal.load( Marshal.dump(p_hash.dig(*l_linked_path)))
    l_hash_resolved.merge!(l_data)
    l_linked_keys = l_hash_resolved.select { |a_key| a_key.to_s.start_with?('_') }
    l_linked_keys.each_key do |a_linked_key|
      l_linked_key = a_linked_key.to_s[1..-1].to_sym
      l_linked_value = l_hash_resolved.delete(a_linked_key)
      LOGGER.debug(__method__){ "Merging linked content from #{a_linked_key}: '#{l_linked_value}' to #{l_linked_key}: '#{l_hash_resolved[l_linked_key]}'" }
      if l_linked_value.is_a?(Hash) && l_hash_resolved[l_linked_key].is_a?(Hash)
        l_hash_resolved[l_linked_key].merge!(l_linked_value)
      else
        l_hash_resolved[l_linked_key] = l_linked_value
      end
    end
    return l_hash_resolved
  end
  # XXX: 'deep' expansion made thanks to recursion ;)
  l_hash_path.push(nil)
  l_data.each do |a_key, a_value|
    l_hash_path[-1] = a_key
    l_data[a_key] = resolve(p_hash, l_hash_path) if a_value.is_a?(Hash) && !a_value.empty?
  end
  l_hash_path.pop
  l_data
end
