SimpleCov.start do
  #add_filter "/.bundle/"
  add_filter %r{^/\..*/}
  add_filter "/spec"
end
